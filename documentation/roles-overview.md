# Roles overview

This file gives an overview of roles in this Self-Description (SD) group, and assigns collaborators to these. 

## Roles

__SD Supplier__:
Own/manages any asset (node/participant/service/...) to be described, and can provide information about these in any form.

__SD Developer__:
Implements SDs on a technical level, namely RDF/turtle, JSON-LD, or SHACL shapes.

__Adviser__:
Brings valuable input to this group, in terms of input/requirements or other connection to other workstreams.

## Roles assignment

The list is below is ordered alphabetically by the __Person__ column.
Please add a link to you Coyo profile so that others can connect with you. In the Coyo profile, please fill out social media like LinkedIn, if possible.

| Person | Organization | SD Supplier | SD Developer | Adviser | Usage (UI/Trust/Catalog/...) |
|:-|:-|:-:|:-:|:-:|:-:|
| Adam Smith | HiSolutions | | | X||
| Alexander Naumenko | AI4BD | X |  |  |  |
| Anja Strunk | Cloud&Heat | | | | |
| Antonin Anchisi |  |  X |  | X |  |
| Adam Smith | HiSolutions |  |  | X |  |
| Arnaud Braud | Orange | X | | | | |
| Berthold Maier | T-Systems |  | X | X |  |
| [Christoph Lange](https://gaia.coyocloud.com/profile/christoph-lange) | Fraunhofer FIT |  | X |  |  |
| Cornelius Hald | | X |  | X |  |
| Dennis Kubitza | Fraunhofer IAIS |  | X |  |  |
| Detlef Hühnlein |  ecsec | X |  | X | X |
| Diamantis Ververis | Bosch | |  | X |  |
| [Dominik Stingl](https://gaia.coyocloud.com/profile/dominik-stingl/info) | DE-CIX | X | | | |
| [Felix Mönckemeyer](https://gaia.coyocloud.com/profile/felix-moenckemeyer/info) | Senseering GmbH | X | X | X |  |
| Gebhard Marent |   | |  | X |  |
| [Giuditta Del Buono](https://www.linkedin.com/in/giudittadelbono/) | Netalia |   |  | X |  |
| [Günter Eggers](https://gaia.coyocloud.com/profile/guenter-eggers) | NTT GDC | X |  | X |  |
| Harald Wagener |  | X | X |  |  |
| [Heinz Ebensperger](https://gaia.coyocloud.com/profile/heinz-ebensperger) | Salesforce Deutschland GmbH | X |  | X |  |
| Jean Chorin |  | |  |  | X |
| Jens Plogsties |SysEleven  | X |  |  | X |
| Jean Chorin |  | |  |  | X |
| Jens Plogsties |  | X |  |  | X |
| Joerg Heese |  | X | X |  | X |
| Johannes Lipp | Fraunhofer FIT |  | X |  |  |
| Jonas Rebstadt |  |  |  | X |  |
| Jörg Langkau | nicos | | X |  | (X) |
| Josep Blanch |  | X |  |  |  |
| Kurt Garloff | SCS | X | X |  |  |
| Leire Orue-Echevarria | TECNALIA |  | X | X | |
| Maarten Kollenstart | TNO | | X | X | |
| Mark Kühner | SAP |  |  | X |  |
| Markus Ketterl | msg | X | X |  |  |
| [Markus Leberecht](https://gaia.coyocloud.com/profile/markus-leberecht/info) | Intel |  |  | X |  |
| [Martin Swientek](https://gaia.coyocloud.com/profile/martin-swientek/info) | Capgemini |  |  | X |  |
| Martin Voigt | AI4BD | X | X |  |  |
| Matthias Binzer |  | X | X | X |  |
| [Michael Mundt](https://gaia.coyocloud.com/profile/michael-mundt/info) | Esri Deutschland GmbH | X | X |  |  |
| [Mrinal Devadas](https://gaia.coyocloud.com/profile/mrinal-devadas/info) | NetApp Inc. |  |  | X |  |
| Olav Strawe |  | |  | X | X |
| Olivier Tirat | BYO | X |  | X |  |
| [Pierre Gronlier](https://www.linkedin.com/in/pierregronlier/) | OVHcloud | X | X |  |  |
| [Raff Poltronieri](https://www.linkedin.com/in/poltronieri/) | Netalia | X |  | X |  |
| Ronny Reinhardt | Cloud&Heat | X |  | X |  |
| Sebastian Bader | Fraunhofer IAIS |  | X |  |  |
| Sergiu Stejar |  |  | X |  |  |
| Simon Dalmolen | TNO | | | X | |
| [Sven Schillack](https://gaia.coyocloud.com/profile/sven-schillack/info) | 50Hertz Transmission GmbH |   | X | X | X |
| Thomas Feld | |  X |  |  | X |
| Tobias Graf |  |  | X | X | X |
| Ulrich Walter | IBM  | |  | X |  |
| Valeri Parshin | |  |  | X |  |
| Xavier Poisson | HPE |  |  | X | |
| Xuan Jin | Alibaba Cloud |  |  |  |  |
